CompletableFuture is a class that implements
- Future
- and CompletionStage

This is the reason why both are compatible

CompletionStage adds method to chain tasks
CompletableFuture adds more methods

In CompletableFuture API the task has a State which was not the case with Future and ExecutorService

The state may be :
-running
-completed normally
-completed exceptionally

Future

T get()
T get(long timeOut, TimeUnit unit)
void cancel()
boolean isDone()
boolean isCancelled()

CompletableFuture

T join() // may throw an Exception
is same as the get method, the difference is that it does not throw a checked exception, so
we do not have to rub this call in try catch

T getNow(T valueIfAbsent)
checks if the task is done and if that's not the case, we will cancel it and return
the value of absent parameter that is passed

Two methods to force the return value below :-
boolean complete(V value)
Checks if the task is done
-if it is done: then it does nothing
-if it is not, then it completes it and sets the returned value to value
void obtrudeValue(V value)
Checks if the task is done
- if it is done: then forces the returned value to value
-if it is not, then it completes it and sets the returned value to value
void obtrudeValue(V value)
- This should be used in error recovery operations

2 to FOrce an Exception:-
boolean completeExceptionally(Throwable t);
forces the completion if the task is not done (join method will throw an exception)
void obtrudeException(Throwable t)
forces the completion even if the task is done (join or get)

