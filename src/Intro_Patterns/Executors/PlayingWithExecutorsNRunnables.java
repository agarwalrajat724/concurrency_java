package Intro_Patterns.Executors;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PlayingWithExecutorsNRunnables {

    public static void main(String[] args) {
        Runnable task = () -> {
            System.out.println(" I am in Thread : " + Thread.currentThread().getName());
        };

        //ExecutorService executorService = Executors.newSingleThreadExecutor();

//        for (int i = 0; i < 10; i++) {
//            new Thread(task).start();
//        }

        ExecutorService executorService = Executors.newFixedThreadPool(4);
        for (int i = 0; i < 10; i++) {
            executorService.execute(task);
        }

        executorService.shutdown();
    }
}
