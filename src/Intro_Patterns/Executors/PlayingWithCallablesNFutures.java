package Intro_Patterns.Executors;

import java.util.concurrent.*;

public class PlayingWithCallablesNFutures {

    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {
        Callable<String> task = () -> {
            Thread.sleep(300);
            return "I'm in Thread : " + Thread.currentThread().getName();
        };

        Callable<String> task1 = () -> {
            throw new IllegalStateException("Throwing an Exception in Thread " + Thread.currentThread().getName());
        };

        ExecutorService executorService = Executors.newFixedThreadPool(4);

        try {

            for (int i = 0; i < 10; i++) {
                Future<String> future = executorService.submit(task1);
                //System.out.println("Get : " + future.get(100, TimeUnit.MILLISECONDS));
                System.out.println("Get: " + future.get());
//            if (future.isDone()) {
//                future.get();
//            }
            }
        } finally {
            executorService.shutdown();
        }

    }
}


