package Intro_Patterns.Executors;

/**
 * 1) A Thread is created on demand by the user.
 * 2) Once the task is done, the Thread dies.
 * 3) Problem: A Thread is an expensive resource...
 *
 * How can we improve the use of Threads, as resources ?
 * 1) By creating the pools of ready-to-use threads and using them,
 *    insteading of creating the thread with a task as a parameter
 *
 * 2) We will pass a task to a pool of threads, that will execute it
 *
 * I will be the role of the pool of threads to take a task, choose an available
 * thread, pass this task to this Thread and execute it in this Thread.
 *
 * We need to below 2 patterns:
 * 1)-> The first one to create a pool of Threads
 * 2)-> To pass a task to this pool.
 *
 *
 */
public class RunnableThreadDemo {

    public static void main(String[] args) {
        Runnable runnable = () -> System.out.println(" Hello World! ");

        Thread thread = new Thread(runnable);

        thread.start();
    }
}
