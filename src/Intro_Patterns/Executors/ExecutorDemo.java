package Intro_Patterns.Executors;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * A pool of Thread is an instance of the Executor Interface.
 * Only one method in Executor interface
 * public interface Executor {
 *     void execute(Runnable task);
 * }
 *
 * Implementation available in JDK, no need to implement
 *
 * ExecutorService is an extension of Executor // 11 more methods
 *
 * public interface ExecutorServive extends Executor {
 *
 * }
 * The implementation of both interfaces are the same
 *
 * To create implementation of these 2 Interfaces we have a Factory class
 * Executors proposes 20 methods to create executors
 *
 * Builds a pool with only one Thread in it
 * ExecutorService singlrThreadExecutor = Executors.newSingleThreadExecutor();
 *
 * IMP:- The Thread in this pool will be alive as long as this pool is alive.
 *
 * Then, when we pass a task to this ExecutorService, this task will be executed in that
 * Thread and this Thread will now be destroyed when this Task is done.
 *
 * How are we going to free the Threads of this ExecutorService ?
 */
public class ExecutorDemo {

    public void function1() {

    }

    public void function2() {

    }

    public static void main(String[] args) {

        ExecutorDemo executorDemo = new ExecutorDemo();

        Executor executor = Executors.newSingleThreadExecutor();

        Runnable task1 = () -> executorDemo.function1();

        Runnable task2 = () -> executorDemo.function2();

        // Suppose task1 is a really long process
        // Then Task2 has to wait for task1 to complete
        // The Single Thread Executor has a waiting queue to handle that

    }
}
