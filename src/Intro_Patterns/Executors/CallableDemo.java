package Intro_Patterns.Executors;

import java.util.concurrent.*;

/**
 * The Future object is returned by the submit() call in the main Thread
 * The get() call is blocking until the String is available
 * The Future.get() method can raise two exceptions:-
 * - in case the Thread of the executor is interrupted, it throws an InterruptedException (
 *      by issuing a shutdown() command to the Executor).
 * - in case the Task throws an Exception, it is wrapped in an ExecutionException and re-thrown
 */
public class CallableDemo {

    private static String function1() {
        return null;
    }
    public static void main(String[] args) {

        Executor executor = Executors.newSingleThreadExecutor();

        Callable<String> task = () -> function1();

        Future<String> future = ((ExecutorService) executor).submit(task);

        try {
            String result = future.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
