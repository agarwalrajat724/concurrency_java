package Intro_Patterns.Executors;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FirstCompletableFutures {

    public static void main(String[] args) throws InterruptedException {
        int i = 10;
        CompletableFuture.runAsync(() -> {
            try {
                System.out.println("Asynchronous : " + i);
                throw new Exception();
            } catch (Exception e) {

            }
        });
        Thread.sleep(300);

        ExecutorService service = Executors.newSingleThreadExecutor();

        Runnable task = () -> System.out.println("Running Asynchronously : " + Thread.currentThread().getName());

        CompletableFuture.runAsync(task);

        service.shutdown();
    }
}
