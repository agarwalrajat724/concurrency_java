package Intro_Patterns.Executors;

import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

public class CompleteFutureWithSupplier {

    public static void main(String[] args) {

        Supplier<String> supplier = () -> {
            try {
                Thread.sleep(10000);
            }  catch (InterruptedException e) {
                e.printStackTrace();
            }
           return Thread.currentThread().getName();
        };

        CompletableFuture<String> completableFuture = CompletableFuture.supplyAsync(supplier);


        completableFuture.complete("Too Long!!!");

        String result = completableFuture.join();

        System.out.println(result);
    }
}
