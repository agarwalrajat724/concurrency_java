package Intro_Patterns.Executors;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CompletableFutureDemo {

    public static void main(String[] args) {

        ExecutorService executorService = Executors.newSingleThreadExecutor();

        Future<?> future = executorService.submit(() ->  System.out.println("Hello World!!!!") );

        CompletableFuture<?> completableFuture = CompletableFuture.runAsync(
                () -> System.out.println("Hello!!!!"), executorService
        );
    }
}
